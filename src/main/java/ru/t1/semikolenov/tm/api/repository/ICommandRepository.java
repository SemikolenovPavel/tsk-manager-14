package ru.t1.semikolenov.tm.api.repository;

import ru.t1.semikolenov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
