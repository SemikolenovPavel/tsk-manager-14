package ru.t1.semikolenov.tm.api.model;

import java.util.Date;

public interface IHasDateBegin {

    Date getDateBegin();

    void setDateBegin(Date dateBegin);

}
